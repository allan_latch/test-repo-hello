

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class ValidateJSONSchema implements Callable{
	
	private String incomingMessageString;
	private String schemaAsString;
	
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		MuleMessage message = eventContext.getMessage();
		
		try {
			this.incomingMessageString = message.getPayloadAsString();
			this.schemaAsString = message.getProperty("schemaAsString", PropertyScope.SESSION);
			if (ValidationUtils.isJsonValid(this.schemaAsString, this.incomingMessageString)) {
				message.setProperty("isValidMS3MessageSchema", true, PropertyScope.SESSION);
			} else {
				message.setProperty("isValidMS3MessageSchema", false, PropertyScope.SESSION);
			}
		} catch (Exception ex) {
			message.setProperty("isValidMS3MessageSchema", false, PropertyScope.SESSION);
		}

		message.removeProperty("schemaAsString", PropertyScope.SESSION);
		return message;
	}

}
